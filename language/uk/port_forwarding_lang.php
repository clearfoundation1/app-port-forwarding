<?php

$lang['port_forwarding_app_description'] = 'Додаток «Прокидання портів» дозволяє перенаправити порти та дати доступ до систем у вашій локальній мережі з Інтернету.';
$lang['port_forwarding_app_name'] = 'Прокидання портів';
$lang['port_forwarding_feature_is_not_supported'] = 'Функція не підтримується.';
$lang['port_forwarding_port_forwarding'] = 'Переадресація портів';
$lang['port_forwarding_port_range_warning'] = 'Діапазон портів повинен вказуватися від початку одного порту (наприклад, 15000) до кінця іншого порту (наприклад, 15999).';
